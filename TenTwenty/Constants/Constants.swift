//
//  Constants.swift
//  mindera
//
//  Created by Furqan on 18/01/2021.
//

import Foundation


struct Constants {
    
    struct ApiKeys {
        static let TMDB = "5f162c13fac2af980de81141d5439199"
    }
    
    struct URLs {
        static let TMDB_BaseUrl = "https://api.themoviedb.org/3/movie/"
        static let TMDB_Poster_BaseUrl = "https://image.tmdb.org/t/p/original"
    }
    
    struct WebserviceDataFormat {
        static let Json = "json"
    }
    
    

}
