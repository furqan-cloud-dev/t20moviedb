//
//  Utilities.swift
//  mindera
//
//  Created by Furqan on 18/01/2021.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Kingfisher

class Utilities {
    
    static func showAlert(title: String?, message: String?, onViewController: UIViewController?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        if onViewController != nil {
            onViewController!.present(alert, animated: true, completion: nil)
        }
    }
    
    static func showAlert(message: String, onViewController: UIViewController?) {
        showAlert(title: nil, message: message, onViewController: onViewController)
    }
    
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
     static func activityIndicatorView(onView: UIView) -> NVActivityIndicatorView {
        let width: CGFloat = 50.0
          let activity = NVActivityIndicatorView(frame: CGRect(x: onView.frame.size.width/2 - width/2, y: 200, width: width, height: width), type: .ballSpinFadeLoader, color: UIColor.lightGray, padding: 5.0)
        onView.addSubview(activity)
        return activity
    }
    
    
    
    /*
    
    static func setImageWithUrl(imageView: UIImageView, urlString: String) {
        if let url = URL(string: urlString) {
            let processor = DownsamplingImageProcessor(size: imgviewPoster.bounds.size)
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: UIImage(named: "blank-image"), options: [.processor(processor),.scaleFactor(UIScreen.main.scale),.transition(.fade(1))], progressBlock: nil) { (result) in
            }
        }
    }
 
 */
    
    
}
