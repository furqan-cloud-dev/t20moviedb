//
//  FlickrWebservices.swift
//  mindera
//
//  Created by Furqan on 18/01/2021.
//

import Foundation

import Foundation
import Alamofire


enum MovieEndpoints {
    case getPopular(page: Int)
    case getDetail(movieID: Int)
}


extension MovieEndpoints: Endpoint {
    
    var path: String {
        switch self {
            case .getPopular: return "popular"
            case .getDetail(let movieID): return "\(movieID)"
        }
    }


    var method: HTTPMethod {
        switch self {
            case .getPopular: return .get
            case .getDetail: return .get
        }
    }


    var parameters: Dictionary<String,Any>? {
//        let format = Constants.WebserviceDataFormat.Json
        switch self {
            case .getPopular(let page):
                return ["language": "en-US","page": page]
            
            case .getDetail:
                //append_to_response=videos
                return ["language": "en-US", "append_to_response": "videos"]
        }
    }
    
    
}



final class MovieWebservice {
    
    var apiKey: String
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    private func processRequest(endpoint: MovieEndpoints, parameterEncoding: ParameterEncoding, completion:@escaping(WebServiceCompletionBlock)) {
        let url = URL(string: endpoint.fullURL)!
        Webservice(apiKey: apiKey).processRequest(url: url, parameters: endpoint.parameters, httpMethod: endpoint.method, headers: endpoint.headers, encoding: parameterEncoding) { (success, result, error) in
            completion(success,result,error)
        }
    }
    

        
    func getPopular(tag: String, page: Int, completion: @escaping ([Movie]?, Int) -> ()) {
        processRequest(endpoint: .getPopular(page: page), parameterEncoding: URLEncoding.default) { (success, result, error) in
            
            if let json = result as? [String: Any] {
                let totalPages = (json["total_pages"] as? Int) ?? 1
                if let items = json["results"] as? [[String: Any]] {
                    if let jsonData = items.toJSONData() {
                        do {
                            let movies = try JSONDecoder().decode([Movie].self, from: jsonData)
                            completion(movies,totalPages)
                            return
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
            completion(nil,0)
        }
    }
    
    
    
    func getMovieDetail(movieID: Int, completion: @escaping (Movie?) -> ()) {
        processRequest(endpoint: .getDetail(movieID: movieID), parameterEncoding: URLEncoding.default) { (success, result, error) in
            if let jsonDictionary = result as? [String: Any] {
                if let jsonData = jsonDictionary.toJSONData() {
                    do {
                        let movie = try JSONDecoder().decode(Movie.self, from: jsonData)
                        completion(movie)
                        return
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
            completion(nil)
        }
    }
    
    
}
    
    
    
    
    

/*
 
 func getPopular(tag: String, page: Int, completion: @escaping ([Movie]?, Int) -> ()) {
     processRequest(endpoint: .getPopular(page: page), parameterEncoding: URLEncoding.default) { (success, result, error) in
         if let json = result as? [String: Any] {
             let totalPages = (json["total_pages"] as? Int) ?? 1
             if let items = json["results"] as? [[String: Any]] {
                 if items.count > 0 {
                     var movies = [Movie]()
                     for item in items {
                         if let movie = Movie(json: item) {
                             movies.append(movie)
                         }
                     }
                     completion(movies,totalPages)
                     return
                 }
             }
         }
         completion(nil,0)
     }
 }
 
 */


/*
 
 
 func getMovieDetail(movieID: Int, completion: @escaping (Movie?) -> ()) {
     processRequest(endpoint: .getDetail(movieID: movieID), parameterEncoding: URLEncoding.default) { (success, result, error) in
         if let json = result as? [String: Any] {
             if let movie = Movie(json: json) {
                 completion(movie)
                 return
             }
         }
         completion(nil)
     }
 }
 */
