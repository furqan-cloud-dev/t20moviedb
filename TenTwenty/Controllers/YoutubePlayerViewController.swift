//
//  YoutubePlayerViewController.swift
//  TenTwenty
//
//  Created by Furqan on 27/01/2021.
//

import UIKit
import YouTubePlayer

class YoutubePlayerViewController: UIViewController {
    
    @IBOutlet var playerView: YouTubePlayerView!
    
    
    var youtubeVideoID: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadVideo()
        play()
    }
    
    
    func loadVideo() {
        playerView.playerVars = [
            "playsinline": "1",
            "controls": "0",
            "showinfo": "0"
            ] as YouTubePlayerView.YouTubePlayerParameters
        playerView.loadVideoID(youtubeVideoID)
    }
    
    
    func play() {
        if playerView.ready {
            if playerView.playerState != YouTubePlayerState.Playing {
                playerView.play()
            } else {
                playerView.pause()
            }
        }
    }
    
    
    @IBAction func close() {
        playerView.stop()
        dismiss(animated: true, completion: nil)
    }
    

}
