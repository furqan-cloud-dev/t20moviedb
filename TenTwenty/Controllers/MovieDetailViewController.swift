//
//  MovieDetailViewController.swift
//  TenTwenty
//
//  Created by Furqan on 24/01/2021.
//

import UIKit
import NVActivityIndicatorView


class MovieDetailViewController: UITableViewController {
    
    @IBOutlet weak var imgviewPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var btnWatchTrailer: UIButton!

    
    var activity: NVActivityIndicatorView!
    var movie: Movie?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Movie Detail"
        activity = Utilities.activityIndicatorView(onView: self.view)
        btnWatchTrailer.isEnabled = false
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMovieDetail()
    }
    
    
    
    func getMovieDetail() {
        guard let movieID = self.movie?.id else {
            return
        }
        activity.startAnimating()
        MovieWebservice(apiKey: Constants.ApiKeys.TMDB).getMovieDetail(movieID: movieID) { [weak self] (movie) in
            DispatchQueue.main.async {
                self?.activity.stopAnimating()
                if movie != nil {
                    self?.movie = movie!
                    self?.updateViewsWithDataModel(movie: movie!)
                    self?.tableView.layoutSubviews()
                }
            }
        }
    }
    
    
    
    func updateViewsWithDataModel(movie: Movie) {
        lblTitle.text = movie.title
        lblOverview.text = movie.overview
        lblReleaseDate.text = movie.releaseDate
        if let genres = movie.genres {
            lblGenres.text = genresStr(genres: genres)
        }
        if let posterImageURL = movie.posterImageUrl {
            imgviewPoster.setImageWithUrl(urlString: posterImageURL)
        }
        btnWatchTrailer.isEnabled = true        
    }
    
    
    func genresStr(genres: [Genre]) -> String {
        let namesArray = genres.map { $0.name! }
        let str = namesArray.joined(separator: ", ")
        return str
    }
    
    
    @IBAction func watchTrailerTapped(_ sender: Any) {
        guard let videos = self.movie?.videos else {
            return
        }
        
        if videos.count > 0 {
            let video = videos[0]
            if let youtubeVideoID = video.key {
                let storyboard = UIStoryboard(name: "VideoPlayer", bundle: nil)
                let youtubePlayerVC = storyboard.instantiateViewController(withIdentifier: "YoutubePlayerViewController") as! YoutubePlayerViewController
                youtubePlayerVC.youtubeVideoID = youtubeVideoID
                self.present(youtubePlayerVC, animated: true, completion: nil)
                
            }
        }
    }
    
    
    
    
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
////        if indexPath.row == 5 {
////            return UITableView.automaticDimension
////        }
////        return super.tableView(tableView, heightForRowAt: indexPath)
//    }
    
}
