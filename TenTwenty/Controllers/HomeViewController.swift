//
//  HomeViewController.swift
//  mindera
//
//  Created by Furqan on 18/01/2021.
//

import UIKit
import NVActivityIndicatorView

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableviewMovies: UITableView!
    
    var movies = [Movie]()
    var activity: NVActivityIndicatorView!
    var isDataUpdateRequire = true
    var currentPage = 1
    var totalPages = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Movie Catalog"
        tableviewMovies.tableFooterView = UIView()
        activity = Utilities.activityIndicatorView(onView: self.view)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isDataUpdateRequire {
            isDataUpdateRequire = false
            getPopularMovies(page: currentPage)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        isDataUpdateRequire = true
        super.viewWillDisappear(animated)
    }
    
    
    
    func getPopularMovies(page: Int) {
        activity.startAnimating()
        MovieWebservice(apiKey: Constants.ApiKeys.TMDB).getPopular(tag: "kitten", page: page) { [weak self] (movies,totalPages) in
            DispatchQueue.main.async {
                self?.activity.stopAnimating()
                if movies != nil {
                    self?.movies.append(contentsOf: movies!)
                    self?.currentPage += 1
                    self?.totalPages = totalPages
                    self?.tableviewMovies.reloadData()
                }
            }
        }
    }
    
    
    
       
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource, SeeMoreTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == movies.count - 1 {
            // see more cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeeMoreTableViewCell", for: indexPath) as! SeeMoreTableViewCell
            cell.delegate = self
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
        let movie = movies[indexPath.row]
        let movieViewModel = MovieViewModel(with: movie)
        cell.configure(with: movieViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let movieDetailVC = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        let movie = movies[indexPath.row]
        movieDetailVC.movie = movie
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
    
    
    func seeMoreTableViewCellTapped(seeMoreTableViewCell: SeeMoreTableViewCell) {
        
        if currentPage <= totalPages {
            getPopularMovies(page: currentPage)
        }
    }
    
}
