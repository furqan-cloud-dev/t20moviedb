//
//  WebserviceResponseSerializer.swift
//  Absher
//
//  Created by Muhammad Furqan khan on 2/25/19.
//  Copyright © 2019 Muhammad Furqan khan. All rights reserved.
//

import Foundation

final class WebserviceResponseSerializer {
    
    
    func getItems(json: [String: Any]) -> (items: [[String: Any]]?, totalPages: Int) {
        if let response = json["Response"] as? [String: Any] {
            if let body = response["Body"] as? [String: Any] {
                if let items = body["Items"] as? [[String: Any]] {
                    if items.count > 0 {
                        var totalPages = 1
                        if let metaData = body["MetaData"] as? [String: Any] {
                            if let total_pages = metaData["TotalPages"] as? String {
//                                if let totalPagesIntValue = total_pages.int {
//                                    if totalPagesIntValue != 0 {
//                                        totalPages = totalPagesIntValue
//                                    }
//                                }
                            }
                        }
                        return (items, totalPages)
                    }
                }
            }
        }
        return (nil,1)
    }
    
    
    func getMediaItems(json: [String: Any]) -> (items: [[String: Any]]?, totalPages: Int) {
        if let body = json["Body"] as? [String: Any] {
            if let items = body["Items"] as? [[String: Any]] {
                if items.count > 0 {
                    var totalPages = 1
                    if let metaData = body["MetaData"] as? [String: Any] {
                        if let total_pages = metaData["TotalPages"] as? String {
//                            if let totalPagesIntValue = total_pages.int {
//                                if totalPagesIntValue != 0 {
//                                    totalPages = totalPagesIntValue
//                                }
//                            }
                        }
                    }
                    return (items, totalPages)
                }
            }
        }
        return (nil,1)
    }
    
    
    
    func getCodeDescription(json: [String: Any]) -> (code: String?, description: String?) {
        if let response = json["Response"] as? [String: Any] {
            if let body = response["Body"] as? [String: Any] {
                let code = body["Code"] as? String
                var description = body["EnDescription"] as? String
                
                return (code,description)
            }
        }
        return (nil,nil)
    }
    
    
    
    static func getResponseStatus(json: [String: Any]) -> (status: Bool?, message: String?) {
        let status = json["status"] as? Bool
        let message = json["message"] as? String
        return (status,message)
    }
    
    
    
}
