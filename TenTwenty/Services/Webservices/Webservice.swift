//
//  Webservice.swift
//  NetworkLayerExpo
//
//  Created by Muhammad Furqan khan on 1/27/19.
//  Copyright © 2019 Muhammad Furqan khan. All rights reserved.
//

import Foundation
import Alamofire



typealias WebServiceCompletionBlock = (_ success:Bool, _ result: Any?, _ error: Error?) -> Void


/*
 - Use Final
 - The final keyword is a restriction on a class, method, or property that indicates that the declaration cannot be overridden. This allows the compiler to safely bypass dynamic dispatch indirection to optimize the performance
 
 */

final class Webservice {
    
    var apiKey: String
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    
    func processRequest(url: URL, parameters: Dictionary<String,Any>?, httpMethod: HTTPMethod, headers: HTTPHeaders?,encoding: ParameterEncoding = JSONEncoding.default, completion:@escaping(WebServiceCompletionBlock)) {
        
        var param = parameters ?? [String: Any]()
        param["api_key"] = apiKey
      
      
        #if DEBUG
            WebServiceLogger.logRequestParameters(url: url, parameters: param, httpMethod: httpMethod, headers: headers)
        #endif
        
        URLCache.shared.removeAllCachedResponses()
        AF.request(url, method: httpMethod, parameters: param, encoding: encoding, headers: headers).responseJSON { dataResponse in
            
            
            #if DEBUG
            WebServiceLogger.logResponse(dataResponse: dataResponse)
            #endif
            
            if let statusCode = dataResponse.response?.statusCode {
                if statusCode == 200 { //OK
                    if dataResponse.error == nil {
                        let result = try? dataResponse.result.get()
                        completion(true,result,nil)
                        return
                    }
                }
                else if statusCode == 400 { // Bad Request
                    if let result = dataResponse.value as? [String: Any] {
                        if let errorMessage = result["error_description"] as? String {
                             let error = NSError(domain:"", code:400, userInfo:[ NSLocalizedDescriptionKey: errorMessage])
                            completion(false,nil,error)
                            return
                        }
                    }
                }
            }
            completion(false,nil,dataResponse.error)
        }
    }
    
}
