//
//  WebserviceConfigurations.swift
//  NetworkLayerExpo
//
//  Created by Muhammad Furqan khan on 1/27/19.
//  Copyright © 2019 Muhammad Furqan khan. All rights reserved.
//

import Foundation
import Alamofire


enum WebServiceEnvironment {
    case development, production
    
    func getString() -> String {
        switch self {
            case .development: return "DEV"
            case .production: return "PROD"
        }
    }
}


final class WebServiceConfiguration {
   
    static func getEnviroment() -> WebServiceEnvironment {
        return .development
    }
}


protocol Endpoint {
    var baseURL: String { get } // https://server/api/1.0/
    var path: String { get } // /users/
    var fullURL: String { get } // This will automatically be set. https://server/api/1.0/user/
    var method: HTTPMethod { get } // .get
    var parameters: Dictionary<String,Any>? { get }
    var headers: HTTPHeaders { get } // ["Authorization" : "Bearer SOME_TOKEN"]
}


extension Endpoint { // global settings
    
    var baseURL: String {
        if WebServiceConfiguration.getEnviroment() == .production {
            return Constants.URLs.TMDB_BaseUrl
        }
        else {
            return Constants.URLs.TMDB_BaseUrl
        }
    }
    
    var fullURL: String {
        return baseURL + path
    }
    
    var headers: HTTPHeaders {
        var httpHeaders = HTTPHeaders()
        httpHeaders.add(HTTPHeader.contentType("application/json"))
        httpHeaders.add(HTTPHeader.accept("application/json"))
        return httpHeaders
    }
    
    
    
}
