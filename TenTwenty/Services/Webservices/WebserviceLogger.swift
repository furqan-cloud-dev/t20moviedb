//
//  WebserviceLogger.swift
//  NetworkLayerExpo
//
//  Created by Muhammad Furqan khan on 1/27/19.
//  Copyright © 2019 Muhammad Furqan khan. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceLogger {
    
    static func logRequestParameters(url: URL, parameters: Dictionary<String,Any>?, httpMethod: HTTPMethod, headers: HTTPHeaders?) {
        
        print("\n********",WebServiceConfiguration.getEnviroment().getString()," : ",httpMethod.rawValue," ",url," ","********")
        print("RQ_HEADERS {")
        
        headers?.forEach({ (header) in
            print(header.name + " : " + header.value)
        })
//        headers?.forEach { print("\($0): \($1)") }
        print("}")
        print("\nRQ_PARAMETERS {")
        parameters?.forEach { print("\($0): \($1)") }
        print("}")
        print("******************************************************")
    }
    
//    static func logResponse(dataResponse: DataResponse<Any,Error>) {
    static func logResponse(dataResponse: AFDataResponse<Any>) {

        print(dataResponse.request as Any)  // original URL request
        print(dataResponse.response as Any) // URL response
        print(dataResponse.data as Any)     // server data
        print(dataResponse.result)   // result of response serialization
        debugPrint(dataResponse.metrics as Any)
        debugPrint(dataResponse as Any)
        print(dataResponse.result)
        print(dataResponse.error as Any)

//        print(dataResponse.result.value as Any)
//        print(dataResponse.result.error as Any)
        
        if let error = dataResponse.error {
            if let error = error as? AFError {
                switch error {
                case .invalidURL(let url):
                    print("Invalid URL: \(url) - \(error.localizedDescription)")
                case .parameterEncodingFailed(let reason):
                    print("Parameter encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .multipartEncodingFailed(let reason):
                    print("Multipart encoding failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .responseValidationFailed(let reason):
                    print("Response validation failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                    
                
                    
                    switch reason {
                        case .dataFileNil, .dataFileReadFailed:
                            print("Downloaded file could not be read")
                        case .missingContentType(let acceptableContentTypes):
                            print("Content Type Missing: \(acceptableContentTypes)")
                        case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                            print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                        case .unacceptableStatusCode(let code):
                            print("Response status code was unacceptable: \(code)")
                        case .customValidationFailed(let error):
                            print(error.localizedDescription)
                    }
                case .responseSerializationFailed(let reason):
                    print("Response serialization failed: \(error.localizedDescription)")
                    print("Failure Reason: \(reason)")
                case .createUploadableFailed(let error): break
                    
                case .createURLRequestFailed(let error): break
                    
                case .downloadedFileMoveFailed(let error, let source, let destination): break
                    
                case .explicitlyCancelled: break
                    
                case .parameterEncoderFailed(let reason): break
                    
                case .requestAdaptationFailed(let error): break
                
                case .requestRetryFailed(let retryError, let originalError): break
                    
                case .serverTrustEvaluationFailed(let reason): break
                    
                case .sessionDeinitialized: break
                    
                case .sessionInvalidated(let error): break
                    
                case .sessionTaskFailed(let error): break
                    
                case .urlRequestValidationFailed(let reason): break
                    
                }
                
                print("Underlying error: \(String(describing: error.underlyingError))")
            } else if let error = error as? URLError {
                print("URLError occurred: \(error)")
            } else {
                print("Unknown error: \(error)")
            }
        }
    }
    
    
    
    
    
}
