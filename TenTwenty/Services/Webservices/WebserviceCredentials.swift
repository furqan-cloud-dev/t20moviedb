//
//  WebserviceCredentials.swift
//  NetworkLayerExpo
//
//  Created by Muhammad Furqan khan on 1/28/19.
//  Copyright © 2019 Muhammad Furqan khan. All rights reserved.
//

import Foundation


class WebserviceCredentials {
    
    static let shared = WebserviceCredentials()
    
    public private(set) var accessToken: String?
    public private(set) var refreshToken: String?

    private init() {
        
    }

    
    
    func isUserLoggedIn() -> Bool {
        if accessToken != nil {
            return true
        }
        return false
    }
    
}
