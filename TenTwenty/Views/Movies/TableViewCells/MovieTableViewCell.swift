//
//  FeaturedStoryTableViewCell.swift
//  Fiftore
//
//  Created by Furqan on 11/12/2019.
//  Copyright © 2019 fiftore. All rights reserved.
//

import UIKit
import Kingfisher



class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgviewPoster: UIImageView!


    var indexPath: IndexPath!
//    var delegate: FeaturedStoryTableViewCellDelegate?


   override func awakeFromNib() {
       super.awakeFromNib()
       // Initialization code

   }
    
    func configure(with viewModel: MovieViewModel) {
        self.lblTitle.text = viewModel.title

        if let posterImageURL = viewModel.posterImageUrl {
            imgviewPoster.setImageWithUrl(urlString: posterImageURL)
        }
    }
       
       
    
   /*
   func updateViewWithDataModel(movie: Movie, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.lblTitle.text = movie.title

        if let posterImageURL = movie.posterImageUrl {
            if let url = URL(string: posterImageURL) {
                let processor = DownsamplingImageProcessor(size: imgviewPoster.bounds.size)
                imgviewPoster.kf.indicatorType = .activity
                imgviewPoster.kf.setImage(with: url, placeholder: UIImage(named: "blank-image"), options: [.processor(processor),.scaleFactor(UIScreen.main.scale),.transition(.fade(1))], progressBlock: nil) { (result) in
                }
            }
        }
   }
 
 */
       
    

    
    
    

}
