//
//  SeeMoreTableViewCell.swift
//  TenTwenty
//
//  Created by Furqan on 24/01/2021.
//

import UIKit

protocol SeeMoreTableViewCellDelegate: class {
    func seeMoreTableViewCellTapped(seeMoreTableViewCell: SeeMoreTableViewCell)
}

class SeeMoreTableViewCell: UITableViewCell {
    
    weak var delegate: SeeMoreTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func seeMoreTapped() {
        self.delegate?.seeMoreTableViewCellTapped(seeMoreTableViewCell: self)
    }

}
