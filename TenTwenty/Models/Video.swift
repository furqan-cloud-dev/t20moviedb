//
//  Video.swift
//  TenTwenty
//
//  Created by Furqan on 27/01/2021.
//

import Foundation


struct Video: Codable {
    let id: String?
    let key: String?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case key = "key"
        case name = "name"
    }

    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        key = try container.decode(String.self, forKey: .key)
        name = try container.decode(String.self, forKey: .name)
    }
}
