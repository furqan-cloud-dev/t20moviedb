//
//  Flickr.swift
//  mindera
//
//  Created by Furqan on 18/01/2021.
//

import Foundation

struct Movie: Codable {
    let id: Int
    let title: String?
    let overview: String?
    let posterPath: String?
    let posterImageUrl: String?
    let releaseDate: String?
    let genres: [Genre]?
    let videos: [Video]?

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case overview = "overview"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case genres = "genres"
        case videosResults = "videos"
    }
    
    private enum VideoCodingKeys: String, CodingKey {
        case videos = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        overview = try container.decode(String.self, forKey: .overview)
        posterPath = try container.decode(String.self, forKey: .posterPath)
        releaseDate = try container.decode(String.self, forKey: .releaseDate)
        genres = try container.decodeIfPresent([Genre].self, forKey: .genres)
        
        let videosResults = try? container.nestedContainer(keyedBy: VideoCodingKeys.self, forKey: .videosResults)
        videos = try videosResults?.decodeIfPresent([Video].self, forKey: .videos)

        if posterPath != nil {
            self.posterImageUrl = Constants.URLs.TMDB_Poster_BaseUrl + posterPath!
        }
        else {
            self.posterImageUrl = nil
        }
    }
    
    
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var videosResults = container.nestedContainer(keyedBy: VideoCodingKeys.self, forKey: .videosResults)
        try videosResults.encode(videos, forKey: .videos)
    }
}




/*
extension Movie {
    init?(json: [String: Any]) {
        guard let id = json["id"] as? Int else { return nil }
    
        self.id = id
        self.title = json["title"] as? String
        self.overview = json["overview"] as? String
        
        if let posterPath = json["poster_path"] as? String {
            self.posterImageUrl = Constants.URLs.TMDB_Poster_BaseUrl + posterPath
        }
        else {
            self.posterImageUrl = nil
        }
    }
}

 */
