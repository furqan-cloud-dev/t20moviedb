//
//  Extensions.swift
//  TenTwenty
//
//  Created by Furqan on 26/01/2021.
//

import Foundation
import UIKit
import Kingfisher

extension Collection where Iterator.Element == [String : Any] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String : Any]],
            let data = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: data, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
    
    
    func toJSONData(options: JSONSerialization.WritingOptions = .prettyPrinted) -> Data? {
        if let arr = self as? [[String : Any]],
           let data = try? JSONSerialization.data(withJSONObject: arr, options: options) {
            return data
        }
        return nil
    }
    
}



extension Dictionary {
    func toJSONData(options: JSONSerialization.WritingOptions = .prettyPrinted) -> Data? {
        if let dict = self as? [String : Any],
           let data = try? JSONSerialization.data(withJSONObject: dict, options: options) {
            return data
        }
        return nil
    }
}



extension UIImageView {
    
    func setImageWithUrl(urlString: String) {
        if let url = URL(string: urlString) {
            let processor = DownsamplingImageProcessor(size: self.bounds.size)
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url, placeholder: UIImage(named: "blank-image"), options: [.processor(processor),.scaleFactor(UIScreen.main.scale),.transition(.fade(1))], progressBlock: nil) { (result) in
            }
        }
    }
    
}
