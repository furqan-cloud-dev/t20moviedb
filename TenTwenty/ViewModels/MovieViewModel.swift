//
//  MovieViewModel.swift
//  TenTwenty
//
//  Created by Furqan on 24/01/2021.
//

import Foundation

/*
 Best practices for MVVM:
  - No import of UIKit - not have any reference to UI elements
  - Its good if not have any business or validation logic - a separate layer for that will be helpful in future integration
 
 */

struct MovieViewModel {
    
    let title: String?
    let posterImageUrl: String?
    
    
    init(with model: Movie) {
        self.title = model.title
        self.posterImageUrl = model.posterImageUrl
    }
}
